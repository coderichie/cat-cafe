# Cat Cafe
This is a project to learn webdevelopment

Hosted at https://cat-cafe.now.sh/

# Running Locally
- Clone this project locally
- Install the latests version of Node.js
- In the command line, got to the cloned project and run these commands
  - `npm install`
  - `npm start`
- Then go to the url specified.
